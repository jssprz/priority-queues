#include "stdafx.h"
#include "CppUnitTest.h"
#include <fstream>
#include <iomanip>      // std::setprecision
#include "experiments.h"
#include <skew-heap-pq.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace priority_queue::skew_heap;

namespace ExperimentsUnitTest
{		
	TEST_CLASS(SkewHeapTests)
	{
		typedef SkewHeap<int> SH;
	public:
		TEST_METHOD(SkewHeapSpecialSortTest) {
			Logger::WriteMessage("SkewHeapSpecialSortTest started");
			auto report_file = ofstream("tests/SkewHeapSpecialSortTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			auto fq = new SH();
			special_sort_test(report_file, fq);
			delete fq;
		}

		TEST_METHOD(SkewHeapSortInsertingTest) {
			Logger::WriteMessage("SkewHeapSortInsertingTest started");
			auto report_file = ofstream("tests/SkewHeapSortInsertingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			sort_inserting_test<SH>(report_file);
		}

		TEST_METHOD(SkewHeapInsertAndMeldingTest) {
			Logger::WriteMessage("SkewHeapInsertAndMeldingTest started");
			auto report_file = ofstream("tests/SkewHeapInsertAndMeldingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			insert_and_meld_test<SH>(report_file);
		}

		TEST_METHOD(SkewHeapMeldTest) {
			Logger::WriteMessage("SkewHeapMeldTest started");
			auto report_file = ofstream("tests/SkewHeapMeldTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			meld_test<SH>(report_file);
		}
	};

	
}