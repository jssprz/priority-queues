#pragma once

#include "CppUnitTest.h"
#include "timer.h"
#include <algorithm>    // std::make_heap, std::pop_heap, std::push_heap, std::sort_heap
#include <numeric>		// std::accumulate
#include <deque>
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

#define SORT_TRIALS 8
#define INSERT_MELD_TRIALS 8
#define MELD_TRIALS 8
#define MSG(msg) [&]{ std::wstringstream _s; _s << msg; return _s.str(); }().c_str()

template<typename PQ>
void special_sort_test(ofstream &report_file, PQ *queue) {
	DECLARE_TIMING(creation_timer);
	DECLARE_TIMING(extract_all_timer);

	vector<int> data(33554432/*2097152*/);
	std::iota(std::begin(data), std::end(data), 0);
	
	srand(SORT_TRIALS); //to generate the same trials in all experiments

	auto create_times = vector<vector<double>>(SORT_TRIALS, vector<double>()),
		extract_all_times = vector<vector<double>>(SORT_TRIALS, vector<double>()),
		subtotal_times = vector<vector<double>>(SORT_TRIALS, vector<double>());

	for (int trial = 0; trial < SORT_TRIALS; trial++) {
		Logger::WriteMessage(("Executing trial: " + to_string(trial+1) + " of " + to_string(SORT_TRIALS)).c_str());
		random_shuffle(data.begin(), data.end());

		int n = 32768;
		for (int i = 15; i <= 25; i++) {
			Logger::WriteMessage(("\tN: " + to_string(n)).c_str());

			auto data_subset = vector<int>(data.begin(), data.begin() + n);
			START_TIMING(creation_timer);
			queue->create(data_subset);
			STOP_TIMING(creation_timer);

			create_times[trial].push_back(GET_TIMING(creation_timer));

			START_TIMING(extract_all_timer);
			auto sorted = queue->extract_all();
			STOP_TIMING(extract_all_timer);

			extract_all_times[trial].push_back(GET_TIMING(extract_all_timer));
			subtotal_times[trial].push_back(GET_TIMING(creation_timer) + GET_TIMING(extract_all_timer));

			//check data is correct
			Assert::IsTrue(sorted.size() == n, MSG("Data is not complete"));
			for (vector<int>::iterator it = sorted.begin() + 1; it != sorted.end(); ++it)
				Assert::IsTrue(*it < *(it - 1), MSG("Data is not sorted"));

			n = n << 1;
		}
	}

	int n = 32768;
	for (int i = 15; i <= 25; i++) {
		report_file << "***SPECIAL SORT (n=" << n << ")***" << endl;
		double avg_create_time = 0, avg_extract_all_time = 0, avg_subtotal_time = 0;
		for (int j = 0; j < SORT_TRIALS; j++) {
			avg_create_time += create_times[j][i-15];
			avg_extract_all_time += extract_all_times[j][i-15];
			avg_subtotal_time += create_times[j][i-15] + extract_all_times[j][i-15];
		}
		avg_create_time /= SORT_TRIALS;
		avg_extract_all_time /= SORT_TRIALS;
		avg_subtotal_time /= SORT_TRIALS;

		double sd_create_time = 0, sd_extract_all_time = 0, sd_subtotal_time = 0;
		for (int j = 0; j < SORT_TRIALS; j++) {
			sd_create_time += (create_times[j][i - 15] - avg_create_time) * (create_times[j][i - 15] - avg_create_time);
			sd_extract_all_time += (extract_all_times[j][i - 15] - avg_extract_all_time) * (extract_all_times[j][i - 15] - avg_extract_all_time);
			sd_subtotal_time += (subtotal_times[j][i - 15] - avg_subtotal_time) * (subtotal_times[j][i - 15] - avg_subtotal_time);
		}
		sd_create_time = sqrt(sd_create_time / SORT_TRIALS);
		sd_extract_all_time = sqrt(sd_extract_all_time / SORT_TRIALS);
		sd_subtotal_time = sqrt(sd_subtotal_time / SORT_TRIALS);

		report_file << "\tcreate heap:\t\t" << avg_create_time * 1000 << "(+-" << sd_create_time * 1000 << ")ms" << endl;
		report_file << "\textract all:\t\t" << avg_extract_all_time * 1000 << "(+-" << sd_extract_all_time * 1000 << ")ms" << endl;
		report_file << "\ttotal (n=" << n << "):\t\t" << (avg_subtotal_time) * 1000 << "(+-" << sd_subtotal_time * 1000 << ")ms" << endl << endl;

		n = n << 1;
	}
}

template<typename PQ>
void sort_inserting_test(ofstream &report_file) {
	DECLARE_TIMING(insertion_timer);
	DECLARE_TIMING(extraction_timer);

	vector<int> data(33554432/*2097152*/);
	std::iota(std::begin(data), std::end(data), 0);
	
	srand(SORT_TRIALS); //to generate the same trials in all experiments

	auto insert_all_times = vector<vector<double>>(SORT_TRIALS, vector<double>()),
		insert_each_times = vector<vector<double>>(SORT_TRIALS, vector<double>()),
		extract_all_times = vector<vector<double>>(SORT_TRIALS, vector<double>()),
		extract_each_times = vector<vector<double>>(SORT_TRIALS, vector<double>()),
		subtotal_times = vector<vector<double>>(SORT_TRIALS, vector<double>());

	for (int trial = 0; trial < SORT_TRIALS; trial++) {
		Logger::WriteMessage(("Executing trial: " + to_string(trial+1) + " of " + to_string(SORT_TRIALS)).c_str());
		random_shuffle(data.begin(), data.end());

		int n = 32768;
		for (int i = 15; i <= 25; i++) {
			Logger::WriteMessage(("N: " + to_string(n)).c_str());

			auto queue = new PQ();
			for (int j = 0; j < n; j++) {
				START_TIMING(insertion_timer);
				queue->insert(data[j]);
				STOP_TIMING(insertion_timer);
			}

			insert_all_times[trial].push_back(GET_TOTAL_TIME(insertion_timer));
			insert_each_times[trial].push_back(GET_AVERAGE_TIMING(insertion_timer));

			vector<int> sorted;
			sorted.reserve(n);
			for (int i = 0; i < n; i++) {
				START_TIMING(extraction_timer);
				sorted.push_back(queue->extract_next());
				STOP_TIMING(extraction_timer);
			}

			extract_all_times[trial].push_back(GET_TOTAL_TIME(extraction_timer));
			extract_each_times[trial].push_back(GET_AVERAGE_TIMING(extraction_timer));

			//check data is sorted
			Assert::IsTrue(queue->count() == 0, MSG("The queue is not empty"));
			Assert::IsTrue(sorted.size() == n, MSG("The extracted data is not complete"));
			for (vector<int>::iterator it = sorted.begin() + 1; it != sorted.end(); ++it)
				Assert::IsTrue(*it < *(it - 1), MSG("Data is not sorted"));

			delete queue;

			subtotal_times[trial].push_back(GET_TOTAL_TIME(insertion_timer) + GET_TOTAL_TIME(extraction_timer));

			CLEAR_TIMING(insertion_timer);
			CLEAR_TIMING(extraction_timer);

			n = n << 1;
		}
	}

	int n = 32768;
	for (int i = 15; i <= 25; i++) {
		report_file << "***SORT INSERTING (n=" << n << ")***" << endl;
		double avg_insert_all_time = 0, avg_insert_each_time = 0, avg_extract_all_time = 0, avg_extract_each_time = 0, avg_subtotal_time = 0;
		for (int j = 0; j < SORT_TRIALS; j++) {
			avg_insert_all_time += insert_all_times[j][i-15];
			avg_insert_each_time += insert_each_times[j][i-15];
			avg_extract_all_time += extract_all_times[j][i-15];
			avg_extract_each_time += extract_each_times[j][i-15];
			avg_subtotal_time += subtotal_times[j][i-15];
		}
		avg_insert_all_time /= SORT_TRIALS;
		avg_insert_each_time /= SORT_TRIALS;
		avg_extract_all_time /= SORT_TRIALS;
		avg_extract_each_time /= SORT_TRIALS;
		avg_subtotal_time /= SORT_TRIALS;

		double sd_insert_all_time = 0, sd_insert_each_time = 0, sd_extract_all_time = 0, sd_extract_each_time = 0, sd_subtotal_time = 0;
		for (int j = 0; j < SORT_TRIALS; j++) {
			sd_insert_all_time += (insert_all_times[j][i - 15] - avg_insert_all_time) * (insert_all_times[j][i - 15] - avg_insert_all_time);
			sd_insert_each_time += (insert_each_times[j][i - 15] - avg_insert_each_time) * (insert_each_times[j][i - 15] - avg_insert_each_time);
			sd_extract_all_time += (extract_all_times[j][i - 15] - avg_extract_all_time) * (extract_all_times[j][i - 15] - avg_extract_all_time);
			sd_extract_each_time += (extract_each_times[j][i - 15] - avg_extract_each_time) * (extract_each_times[j][i - 15] - avg_extract_each_time);
			sd_subtotal_time += (subtotal_times[j][i - 15] - avg_subtotal_time) * (subtotal_times[j][i - 15] - avg_subtotal_time);
		}
		sd_insert_all_time = sqrt(sd_insert_all_time / SORT_TRIALS);
		sd_insert_each_time = sqrt(sd_insert_each_time / SORT_TRIALS);
		sd_extract_all_time = sqrt(sd_extract_all_time / SORT_TRIALS);
		sd_extract_each_time = sqrt(sd_extract_each_time / SORT_TRIALS);
		sd_subtotal_time = sqrt(sd_subtotal_time / SORT_TRIALS);

		report_file << "\tinsert all elements:\t\t" << avg_insert_all_time * 1000 << "(+-" << sd_insert_all_time * 1000 << ")ms" << endl;
		report_file << "\tinsert each element:\t\t" << avg_insert_each_time * 1000 << "(+-" << sd_insert_each_time * 1000 << ")ms" << endl;
		report_file << "\textract all elements:\t\t" << avg_extract_all_time * 1000 << "(+-" << sd_extract_all_time * 1000 << ")ms" << endl;
		report_file << "\textract each element:\t\t" << avg_extract_each_time * 1000 << "(+-" << sd_extract_each_time * 1000 << ")ms" << endl;
		report_file << "\ttotal (n=" << n << "):\t\t" << avg_subtotal_time * 1000 << "(+-" << sd_subtotal_time * 1000 << ")ms" << endl << endl;

		n = n << 1;
	}
}

template<typename PQ>
PQ* insert_meld(ofstream &report_file, vector<int> data, int k) {
	DECLARE_TIMING(creation_timer);
	DECLARE_TIMING(meld_timer);
	report_file << "***INSERT AND MELD (k=" << k << ")***" << endl;

	deque<PQ*> queues;

	//insertion
	int subset_size = data.size() / k;
	for (size_t i = 0; i < k; i++) {
		auto pq = new PQ();
		START_TIMING(creation_timer);
		pq->create(vector<int>(data.begin() + i * subset_size, data.begin() + (i + 1) * subset_size));
		STOP_TIMING(creation_timer);

		queues.push_back(pq);
	}

	report_file << "\ttotal time for creation:\t" << GET_TOTAL_TIME(creation_timer) * 1000 << "ms" << endl;
	report_file << "\taverage time for subset creation:\t" << GET_AVERAGE_TIMING(creation_timer) * 1000 << "ms" << endl;
	
	//melding
	while (queues.size() != 1) {
		auto c1 = queues.front(); queues.pop_front();
		auto c2 = queues.front(); queues.pop_front();

		START_TIMING(meld_timer);
		auto meld = PQ::meld(c1, c2);
		STOP_TIMING(meld_timer);

		queues.push_back(meld);
		delete c1, c2;
	}

	report_file << "\ttotal time for meld:\t" << GET_TOTAL_TIME(meld_timer) * 1000 << "ms" << endl;
	report_file << "\taverage time for meld pairs:\t" << GET_AVERAGE_TIMING(meld_timer) * 1000 << "ms" << endl;

	return queues.front();
}

template<typename PQ>
void insert_and_meld_test(ofstream &report_file) {
	DECLARE_TIMING(creation_timer);
	DECLARE_TIMING(meld_timer);

	vector<int> data(1048576);
	std::iota(std::begin(data), std::end(data), 0);

	srand(INSERT_MELD_TRIALS); //to generate the same trials in all experiments

	random_shuffle(data.begin(), data.end());

	auto std_heap = data;
	make_heap(std_heap.begin(), std_heap.end());
	sort_heap(std_heap.begin(), std_heap.end());

	auto create_times = vector<vector<double>>(INSERT_MELD_TRIALS, vector<double>()),
		create_each_times = vector<vector<double>>(INSERT_MELD_TRIALS, vector<double>()),
		meld_times = vector<vector<double>>(INSERT_MELD_TRIALS, vector<double>()),
		meld_each_times = vector<vector<double>>(INSERT_MELD_TRIALS, vector<double>());

	for (int trial = 0; trial < INSERT_MELD_TRIALS; trial++) {
		Logger::WriteMessage(("Executing trial: " + to_string(trial+1) + " of " + to_string(INSERT_MELD_TRIALS)).c_str());
		
		int k = 1;
		for (int i = 0; i <= 15; i++) {
			Logger::WriteMessage(("\tK: " + to_string(k)).c_str());
			deque<PQ*> queues;

			//insertion
			int subset_size = data.size() / k;
			for (size_t i = 0; i < k; i++) {
				auto pq = new PQ();

				START_TIMING(creation_timer);
				pq->create(vector<int>(data.begin() + i * subset_size, data.begin() + (i + 1) * subset_size));
				STOP_TIMING(creation_timer);

				queues.push_back(pq);
			}

			create_times[trial].push_back(GET_TOTAL_TIME(creation_timer));
			create_each_times[trial].push_back(GET_AVERAGE_TIMING(creation_timer));

			//melding
			while (queues.size() != 1) {
				auto c1 = queues.front(); queues.pop_front();
				auto c2 = queues.front(); queues.pop_front();

				START_TIMING(meld_timer);
				auto meld = PQ::meld(c1, c2);
				STOP_TIMING(meld_timer);

				queues.push_back(meld);
				delete c1;
				delete c2;
			}

			meld_times[trial].push_back(GET_TOTAL_TIME(meld_timer));
			meld_each_times[trial].push_back(GET_AVERAGE_TIMING(meld_timer));

			auto pq = queues.front();
			queues.pop_front();

			//check heap
			for (vector<int>::reverse_iterator it = std_heap.rbegin(); it != std_heap.rend(); ++it) {
				auto value = pq->extract_next();
				Assert::AreEqual(*it, value, MSG("The heap constructed for k=" << k << " is not sorted"));
			}
			delete pq;

			CLEAR_TIMING(creation_timer);
			CLEAR_TIMING(meld_timer);

			k = k << 1;
		}
	}

	int k = 1;
	for (int i = 0; i <= 15; i++) {
		report_file << "***INSERT AND MELD (k=" << k << ")***" << endl;
		double avg_create_time = 0, avg_create_each_time = 0, avg_meld_time = 0, avg_meld_each_time = 0;
		for (int j = 0; j < INSERT_MELD_TRIALS; j++) {
			avg_create_time += create_times[j][i];
			avg_create_each_time += create_each_times[j][i];
			avg_meld_time += meld_times[j][i];
			avg_meld_each_time += meld_each_times[j][i];
		}
		avg_create_time /= INSERT_MELD_TRIALS;
		avg_create_each_time /= INSERT_MELD_TRIALS;
		avg_meld_time /= INSERT_MELD_TRIALS;
		avg_meld_each_time /= INSERT_MELD_TRIALS;

		double sd_create_time = 0, sd_create_each_time = 0, sd_meld_time = 0, sd_meld_each_time = 0;
		for (int j = 0; j < INSERT_MELD_TRIALS; j++) {
			sd_create_time += (create_times[j][i] - avg_create_time) * (create_times[j][i] - avg_create_time);
			sd_meld_time += (meld_times[j][i] - avg_meld_time) * (meld_times[j][i] - avg_meld_time);
			sd_create_each_time += (create_each_times[j][i] - avg_create_each_time) * (create_each_times[j][i] - avg_create_each_time);
			sd_meld_each_time += (meld_each_times[j][i] - avg_meld_each_time) * (meld_each_times[j][i] - avg_meld_each_time);
		}
		sd_create_time = sqrt(sd_create_time / INSERT_MELD_TRIALS);
		sd_meld_time = sqrt(sd_meld_time / INSERT_MELD_TRIALS);
		sd_create_each_time = sqrt(sd_create_each_time / INSERT_MELD_TRIALS);
		sd_meld_each_time = sqrt(sd_meld_each_time / INSERT_MELD_TRIALS);

		report_file << "\tcreate all heaps:\t\t" << avg_create_time * 1000 << "(+-" << sd_create_time * 1000 << ")ms" << endl;
		report_file << "\tcreate each heap:\t\t" << avg_create_each_time * 1000 << "(+-" << sd_create_each_time * 1000 << ")ms" << endl;
		report_file << "\tmelding process:\t\t" << avg_meld_time * 1000 << "(+-" << sd_meld_time * 1000 << ")ms" << endl;
		report_file << "\teach meld:\t\t" << avg_meld_each_time * 1000 << "(+-" << sd_meld_each_time * 1000 << ")ms" << endl;
		report_file << "\ttotal (k=" << k << "):\t\t" << (avg_create_time + avg_meld_time) * 1000 << "(+-" << (sd_create_time + sd_meld_time) * 1000 << ")ms" << endl << endl;
		k = k << 1;
	}
}

template<typename PQ>
void meld_test(ofstream &report_file) {
	DECLARE_TIMING(meld_timer);

	srand(MELD_TRIALS); //to generate the same trials in all experiments

	auto meld_times = vector<vector<double>>(MELD_TRIALS, vector<double>());
	for (size_t trial = 0; trial < MELD_TRIALS; trial++) {
		Logger::WriteMessage(("Executing trial: " + to_string(trial+1) + " of " + to_string(MELD_TRIALS)).c_str());
		
		int n = 32768;
		for (size_t i = 15; i <= 25; i++) {
			Logger::WriteMessage(("\tN: " + to_string(n)).c_str());
			vector<int> data(n);
			std::iota(std::begin(data), std::end(data), 0);
			random_shuffle(data.begin(), data.end());

			vector<int> data2;
			data2.insert(data2.end(), data.begin() + n / 2, data.end());
			data.resize(n / 2);

			auto q1 = new PQ(), q2 = new PQ();
			q1->create(data); vector<int>().swap(data);
			q2->create(data2); vector<int>().swap(data2);

			START_TIMING(meld_timer);
			auto meld = PQ::meld(q1, q2);
			STOP_TIMING(meld_timer);

			meld_times[trial].push_back(GET_TIMING(meld_timer));

			//check data is correct
			auto sorted = meld->extract_all();
			Assert::IsTrue(sorted.size() == n, MSG("Data is not complete"));
			for (vector<int>::iterator it = sorted.begin() + 1; it != sorted.end(); ++it)
				Assert::IsTrue(*it > *(it - 1), MSG("Data is not sorted"));

			delete meld;
			delete q1;
			delete q2;

			n = n << 1;
		}
	}

	int n = 32768;
	for (size_t i = 0; i <= 10; i++) {
		report_file << "***MELD (n=" << n << ")***" << endl;
		double avg_meld_time = 0;
		for (int j = 0; j < MELD_TRIALS; j++)
			avg_meld_time += meld_times[j][i];
		avg_meld_time /= MELD_TRIALS;

		double sd_meld_time = 0;
		for (int j = 0; j < MELD_TRIALS; j++)
			sd_meld_time += (meld_times[j][i] - avg_meld_time) * (meld_times[j][i] - avg_meld_time);
		
		sd_meld_time = sqrt(sd_meld_time / MELD_TRIALS);

		report_file << "\tmelding process:\t\t" << avg_meld_time * 1000 << "(+-" << sd_meld_time * 1000 << ")ms" << endl;
		
		n = n << 1;
	}
}