#include "stdafx.h"
#include "CppUnitTest.h"
#include <fstream>
#include <iomanip>      // std::setprecision
#include "experiments.h"
#include <binary-heap-pq.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace priority_queue::binary_heap;

namespace ExperimentsUnitTest
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		typedef BinaryHeap<int> BH;
		
		TEST_METHOD(BHeapSpecialSortTest) {
			Logger::WriteMessage("BHeapSpecialSortTest started");
			auto report_file = ofstream("tests/BHeapSpecialSortTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			auto bh = new BH();
			special_sort_test(report_file, bh);
			delete bh;
		}

		TEST_METHOD(BHeapSortInsertingTest) {
			Logger::WriteMessage("BHeapSortInsertingTest started");
			auto report_file = ofstream("tests/BHeapSortInsertingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			sort_inserting_test<BH>(report_file);
		}

		TEST_METHOD(BHeapInsertAndMeldingTest) {
			Logger::WriteMessage("BHeapInsertAndMeldingTest started");
			auto report_file = ofstream("tests/BHeapInsertAndMeldingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			insert_and_meld_test<BH>(report_file);
		}

		TEST_METHOD(BHeapMeldTest) {
			Logger::WriteMessage("BHeapMeldTest started");
			auto report_file = ofstream("tests/BHeapMeldTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			meld_test<BH>(report_file);
		}
	};
}