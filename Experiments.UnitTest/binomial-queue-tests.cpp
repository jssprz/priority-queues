#include "stdafx.h"
#include "CppUnitTest.h"
#include <fstream>
#include <iomanip>      // std::setprecision
#include "experiments.h"
#include <binomial-pq.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace priority_queue::binomial_queue;

namespace ExperimentsUnitTest
{		
	TEST_CLASS(BinomialQueueTest)
	{
	public:
		typedef BinomialQueue<int> BQ;
		
		TEST_METHOD(BinomialQueueSpecialSortTest) {
			auto report_file = ofstream("tests/BinomialQueueSpecialSortTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			auto bq = new BQ();
			special_sort_test(report_file, bq);
			delete bq;
		}

		TEST_METHOD(BinomialQueueSortInsertingTest) {
			Logger::WriteMessage("BinomialQueueSortInsertingTest started");
			auto report_file = ofstream("tests/BinomialQueueSortInsertingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			sort_inserting_test<BQ>(report_file);
		}

		TEST_METHOD(BinomialQueueInsertAndMeldingTest) {
			Logger::WriteMessage("BinomialQueueInsertAndMeldingTest started");
			auto report_file = ofstream("tests/BinomialQueueInsertAndMeldingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			insert_and_meld_test<BQ>(report_file);
		}

		TEST_METHOD(BinomialQueueMeldTest) {
			Logger::WriteMessage("BinomialQueueMeldTest started");
			auto report_file = ofstream("tests/BinomialQueueMeldTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			meld_test<BQ>(report_file);
		}
	};
}