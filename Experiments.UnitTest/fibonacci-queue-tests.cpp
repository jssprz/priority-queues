#include "stdafx.h"
#include "CppUnitTest.h"
#include <fstream>
#include <iomanip>      // std::setprecision
#include "experiments.h"
#include <fibonacci-pq.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace priority_queue::fibonacci_queue;

namespace ExperimentsUnitTest
{		
	TEST_CLASS(FibonacciHeapTest)
	{
	public:
		typedef FibonacciQueue<int> FQ;
		
		TEST_METHOD(FibonacciQueueSpecialSortTest) {
			auto report_file = ofstream("tests/FibonacciQueueSpecialSortTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			auto fq = new FQ();
			special_sort_test(report_file, fq);
			delete fq;
		}

		TEST_METHOD(FibonacciQueueSortInsertingTest) {
			Logger::WriteMessage("FibonacciQueueSortInsertingTest started");
			auto report_file = ofstream("tests/FibonacciQueueSortInsertingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			sort_inserting_test<FQ>(report_file);
		}

		TEST_METHOD(FibonacciQueueInsertAndMeldingTest) {
			Logger::WriteMessage("FibonacciQueueInsertAndMeldingTest started");
			auto report_file = ofstream("tests/FibonacciQueueInsertAndMeldingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			insert_and_meld_test<FQ>(report_file);
		}

		TEST_METHOD(FibonacciQueueMeldTest) {
			Logger::WriteMessage("FibonacciQueueMeldTest started");
			auto report_file = ofstream("tests/FibonacciQueueMeldTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			meld_test<FQ>(report_file);
		}
	};

	
}