#include "stdafx.h"
#include "CppUnitTest.h"
#include <fstream>
#include <iomanip>      // std::setprecision
#include "experiments.h"
#include <leftist-heap-pq.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;
using namespace priority_queue::leftist_heap;

namespace ExperimentsUnitTest
{		
	TEST_CLASS(LeftistHeapTest)
	{
		typedef LeftistHeap<int> LH;
	public:
		TEST_METHOD(LeftistHeapSpecialSortTest) {
			Logger::WriteMessage("LeftistHeapSpecialSortTest started");
			auto report_file = ofstream("tests/LeftistHeapSpecialSortTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			auto fq = new LH();
			special_sort_test(report_file, fq);
			delete fq;
		}

		TEST_METHOD(LeftistHeapSortInsertingTest) {
			Logger::WriteMessage("LeftistHeapSortInsertingTest started");
			auto report_file = ofstream("tests/LeftistHeapSortInsertingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			sort_inserting_test<LH>(report_file);
		}

		TEST_METHOD(LeftistHeapInsertAndMeldingTest) {
			Logger::WriteMessage("LeftistHeapInsertAndMeldingTest started");
			auto report_file = ofstream("tests/LeftistHeapInsertAndMeldingTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			insert_and_meld_test<LH>(report_file);
		}

		TEST_METHOD(LeftistHeapMeldTest) {
			Logger::WriteMessage("LeftistHeapMeldTest started");
			auto report_file = ofstream("tests/LeftistHeapMeldTest.report", ios::trunc);
			report_file << fixed << setprecision(5);
			meld_test<LH>(report_file);
		}
	};

	
}