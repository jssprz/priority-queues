#pragma once

namespace priority_queue {
	namespace fibonacci_queue {
		template<typename T>
		class FibonacciQueueNode {
			typedef FibonacciQueueNode<T> Node;

		public:
			FibonacciQueueNode(T key) :key(key) {}

			T key;
			int degree = 0;
			Node *child = NULL, *prev, *next;
		};
	}
}